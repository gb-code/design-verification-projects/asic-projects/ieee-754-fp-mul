library verilog;
use verilog.vl_types.all;
entity fpmul_comb is
    port(
        din1            : in     vl_logic_vector(31 downto 0);
        din2            : in     vl_logic_vector(31 downto 0);
        start           : in     vl_logic;
        dout            : out    vl_logic_vector(31 downto 0);
        done            : out    vl_logic;
        uf              : out    vl_logic;
        \of\            : out    vl_logic;
        nanf            : out    vl_logic;
        inff            : out    vl_logic;
        dnf             : out    vl_logic;
        zf              : out    vl_logic
    );
end fpmul_comb;

library verilog;
use verilog.vl_types.all;
entity fpmul is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        start           : in     vl_logic;
        a               : in     vl_logic_vector(31 downto 0);
        b               : in     vl_logic_vector(31 downto 0);
        p               : out    vl_logic_vector(31 downto 0);
        p_uf            : out    vl_logic;
        p_of            : out    vl_logic;
        p_nanf          : out    vl_logic;
        p_inff          : out    vl_logic;
        p_dnf           : out    vl_logic;
        p_zf            : out    vl_logic;
        done            : out    vl_logic
    );
end fpmul;

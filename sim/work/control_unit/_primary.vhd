library verilog;
use verilog.vl_types.all;
entity control_unit is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        start           : in     vl_logic;
        normal          : in     vl_logic;
        round           : in     vl_logic;
        carry           : in     vl_logic;
        uo_flow         : in     vl_logic;
        map_bit23       : in     vl_logic;
        nan             : in     vl_logic;
        inf             : in     vl_logic;
        zero            : in     vl_logic;
        uflow           : in     vl_logic;
        oflow           : in     vl_logic;
        clr_flags       : out    vl_logic;
        ld_inputs       : out    vl_logic;
        ld_inflags1     : out    vl_logic;
        ld_inflags2     : out    vl_logic;
        ld_prod         : out    vl_logic;
        ld_res_nan      : out    vl_logic;
        ld_res_inf      : out    vl_logic;
        ld_res_zero     : out    vl_logic;
        norm_eap        : out    vl_logic;
        incr_eap        : out    vl_logic;
        calc_round      : out    vl_logic;
        calc_carry      : out    vl_logic;
        norm_map        : out    vl_logic;
        ld_prod_flags   : out    vl_logic;
        ld_prod_uflow   : out    vl_logic;
        ld_prod_oflow   : out    vl_logic;
        ld_res_reg      : out    vl_logic;
        done            : out    vl_logic
    );
end control_unit;

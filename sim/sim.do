vlib work
vmap work work
vlog ../rtl/fpmul_multicycle.v
vlog fpmul_tb.v
vsim -t ps -L work -novopt fpmul_tb
log -r *
do wave.do
run -all

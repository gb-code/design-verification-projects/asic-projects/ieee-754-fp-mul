// -------------------------------------------------------------------------------------------------------------
// File Name     	: fpmul_algorithm_tb.v
// Created By     	: Gaurav
// Date          	: 11/07/16
// Description   	: This module implements testbench for checking fpmul_algorithm.v by generating random seeds. 
// -------------------------------------------------------------------------------------------------------------

`timescale 1ns/1ps
module fpmul_tb();

reg clk,rst;
reg [31:0] din1;
reg [31:0] din2;
reg start;
wire [31:0] dout;
wire done,uf,of,nanf,inff,dnf,zf;

fpmul_multicycle DUT (clk,rst,start,din1,din2,dout,uf,of,nanf,inff,dnf,zf,done);

initial begin
rst = 1;
#22 rst = 0;
end

initial begin
clk = 0;
forever #5 clk = ~clk;
end

integer seed ;
real opa, opb, res, exp_res ;
initial
begin
  din1=0;
  din2=0;
  start=0;
  rst = 1;
  #22 rst = 0;
  @(posedge clk);
  rst = 0;
  #2 start = 1;
  din1 = 32'hb290_9b9a;
  din2 = 32'h002b_9b15;
  opa = fp2real(din1);
  opb = fp2real(din2);
  @(done);
  exp_res = opa * opb ;
  res = fp2real(dout);
  $display("#################################################");
  $display("Operand A = %0f\nOperand B = %0f\nProduct = %0f\nExpected = %0f",opa,opb,res,exp_res);
  @(posedge clk);
  start = 0;
  rst = 1;
  @(posedge clk);
  $finish;
end


function real fp2real;
  input [31:0] A ;
  integer   sign_bit   ;
  integer   exp        ;
  real      expcalc    ;
  integer   floatint   ;
  real      floatreal  ;
  real      floatreal1 ;
  real      floatreal2 ;
  begin
    sign_bit = A[31]  ? -1 : 1 ;
    exp      = A[30:23] - 127 ;
    if(A[30:0] != 0)
    begin
      floatint = A[22:0] ;
      floatreal = 1.0+((floatint*1.0)/(2 ** 23)) ;
      expcalc    = 2.0 ** exp ;
      floatreal1 = floatreal * expcalc ;
      floatreal2 = (sign_bit) * (floatreal1) ;
      fp2real = floatreal2 ;
    end
    else
    begin
      fp2real = 0.0 ;
    end
  end
endfunction
endmodule

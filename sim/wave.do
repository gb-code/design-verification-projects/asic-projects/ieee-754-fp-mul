onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fpmul_tb/clk
add wave -noupdate /fpmul_tb/rst
add wave -noupdate -radix hexadecimal /fpmul_tb/din1
add wave -noupdate -radix hexadecimal /fpmul_tb/din2
add wave -noupdate -radix hexadecimal /fpmul_tb/start
add wave -noupdate -radix hexadecimal /fpmul_tb/dout
add wave -noupdate -radix hexadecimal /fpmul_tb/done
add wave -noupdate -radix hexadecimal /fpmul_tb/uf
add wave -noupdate -radix hexadecimal /fpmul_tb/of
add wave -noupdate -radix hexadecimal /fpmul_tb/nanf
add wave -noupdate -radix hexadecimal /fpmul_tb/inff
add wave -noupdate -radix hexadecimal /fpmul_tb/dnf
add wave -noupdate -radix hexadecimal /fpmul_tb/zf
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {145000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1013250 ps}

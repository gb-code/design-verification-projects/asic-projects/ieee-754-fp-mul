##
## INTRODUCTION

This work aims at the Design and Implementation of a Floating-point Multiplier using multi cycle method to improve the performance of the multiplier. Implementation is targeted on Xilinx’s Nexys 4 DDR board which consists of Artix 7 FPGA.

The design needs to take two floating point operands and a START signal to start the operation. Once the multiplication is done, the DONE signal needs to be asserted for one clock along with the output of multiplication result and the corresponding flags. The design needs to qualify the validity of input operands by checking for the NaN, infinity, zero and DNF flags.

![1.JPG](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/fae3210e-c0c8-4f8a-97d7-1ea4d08ae5b4)

##
## MULTI-CYCLE DESIGN

(fpam_algorithm.v) is a module which instantiates all combinational module which captures the algorithmic steps of floating point multiplier. Although the design synthesizes and takes just one clock cycle for the output to be computed, the frequency of operation would be extremely slow because of this huge combinational delay between two registers

To improve the performance multicycle design is implemented by architecting micro-architecture of datapath and control unit. The datapath which is entirely combinational is controlled by the signals from the control unit. 

![2.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/10e81147-b9b9-43a0-9e51-862da896dfe3)


![3.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/25d8e2f5-058a-4ad8-8632-ccfc1eb0bfe6)

##
## ASM Chart

![4.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/811ef446-f4a5-439f-85d3-e93212e6939a)
![5.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/a49f9343-50a0-40e9-8cb9-74bee9c252e2)
![6.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/5381d7d5-16ad-4fed-9c69-edea60b49f6a)

##
## TESTCASES AND WAVEFORMS


![7.JPG](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/89e97356-236d-42fa-8e29-170c8738d3ce)
![8.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/79160579-47a4-4bec-a634-373bcfd0ab72)


![9.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/84b5bc07-f740-4ef5-bfdc-5570d28e8312)

![10.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/d60c2729-11ac-4f13-b3b5-df25fd8afe8d)

![11.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/fd57c064-4370-4bdd-851d-de16638855e4)

![12.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/19b34d14-3dcc-4d0a-8378-03f56761683e)

![13.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/ffa99823-e965-4abf-9190-ebd5793771dc)

![14.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/e4d1d92b-ea76-4c1f-a766-a859a1a5d5bf)

![15.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/60156f0b-fde1-4c29-8b62-f3245d1387ac)
##
## PERFORMANCE AND TIMING ANALYSIS

•	Single cycle design has a maximum performance of 60 MHz which is 16.694ns as best case timing
•	Multicycle design has a maximum performance of 127.5 MHz which is 7.84ns as best case timing
One can see that the performance has greatly improved in multicycle design as compared to single cycle design of algorithm with a cost in latency and area




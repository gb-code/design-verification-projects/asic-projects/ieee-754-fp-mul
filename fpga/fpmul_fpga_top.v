module fpmul_fpga_top(
  input sys_clk,sys_rst
  );

wire [31:0] a,b,p;
reg [31:0] product;
reg done;
reg uf,of,nanf,inff,dnf,zf;

fpmul_multicycle fpmul (clk,rst,start,a,b,p,p_uf,p_of,p_nanf,p_inff,p_dnf,p_zf,valid);

fpmul_vio vio (clk,product,{uf,of,nanf,inff,dnf,zf,done},a,b,{rst,start});

clk_dcm dcm (sys_clk,clk,sys_rst);

//latch outputs
always@(posedge clk or posedge rst)
begin
if (rst)
  begin
  product <= 32'd0;
  done <= 1'b0;
  uf <= 1'b0;
  of <= 1'b0;
  nanf <= 1'b0;
  inff <= 1'b0;
  dnf <= 1'b0;
  zf <= 1'b0;
  end
else if (valid)
  begin
  product <= p;
  done <= 1'b1;
  uf <= p_uf;
  of <= p_of;
  nanf <= p_nanf;
  inff <= p_inff;
  dnf <= p_dnf;
  zf <= p_zf;
  end
end

endmodule

-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.2 (win64) Build 1577090 Thu Jun  2 16:32:40 MDT 2016
-- Date        : Mon Nov 07 11:43:51 2016
-- Host        : RohitDhage running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/clk_dcm/clk_dcm_stub.vhdl
-- Design      : clk_dcm
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_dcm is
  Port ( 
    sys_clk : in STD_LOGIC;
    clk : out STD_LOGIC;
    resetn : in STD_LOGIC
  );

end clk_dcm;

architecture stub of clk_dcm is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "sys_clk,clk,resetn";
begin
end;

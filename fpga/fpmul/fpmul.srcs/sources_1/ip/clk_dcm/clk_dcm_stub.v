// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (win64) Build 1577090 Thu Jun  2 16:32:40 MDT 2016
// Date        : Mon Nov 07 11:43:51 2016
// Host        : RohitDhage running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/clk_dcm/clk_dcm_stub.v
// Design      : clk_dcm
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_dcm(sys_clk, clk, resetn)
/* synthesis syn_black_box black_box_pad_pin="sys_clk,clk,resetn" */;
  input sys_clk;
  output clk;
  input resetn;
endmodule

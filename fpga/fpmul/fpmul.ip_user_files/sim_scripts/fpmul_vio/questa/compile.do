vlib work
vlib msim

vlib msim/xil_defaultlib

vmap xil_defaultlib msim/xil_defaultlib

vlog -work xil_defaultlib -64 "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/vio_v3_0_12/hdl" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/vio_v3_0_12/hdl" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../fpmul.srcs/sources_1/ip/fpmul_vio/xsdbs_v1_0_2/hdl/verilog" \
"../../../../fpmul.srcs/sources_1/ip/fpmul_vio/sim/fpmul_vio.v" \


vlog -work xil_defaultlib "glbl.v"


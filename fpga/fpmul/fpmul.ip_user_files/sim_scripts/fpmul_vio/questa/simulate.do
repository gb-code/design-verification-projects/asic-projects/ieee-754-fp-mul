onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fpmul_vio_opt

do {wave.do}

view wave
view structure
view signals

do {fpmul_vio.udo}

run -all

quit -force

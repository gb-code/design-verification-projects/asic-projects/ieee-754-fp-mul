// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (win64) Build 1577090 Thu Jun  2 16:32:40 MDT 2016
// Date        : Sun Nov 06 08:48:34 2016
// Host        : RohitDhage running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_stub.v
// Design      : fpmul_vio
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2016.2" *)
module fpmul_vio(clk, probe_in0, probe_in1, probe_out0, probe_out1, probe_out2)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[31:0],probe_in1[6:0],probe_out0[31:0],probe_out1[31:0],probe_out2[1:0]" */;
  input clk;
  input [31:0]probe_in0;
  input [6:0]probe_in1;
  output [31:0]probe_out0;
  output [31:0]probe_out1;
  output [1:0]probe_out2;
endmodule

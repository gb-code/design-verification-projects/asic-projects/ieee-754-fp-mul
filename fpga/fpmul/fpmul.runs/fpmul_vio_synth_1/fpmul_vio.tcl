# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7a100tcsg324-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.cache/wt [current_project]
set_property parent.project_path C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
read_ip -quiet c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio.xci
set_property is_locked true [get_files c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio.xci]

foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}

synth_design -top fpmul_vio -part xc7a100tcsg324-1 -mode out_of_context

rename_ref -prefix_all fpmul_vio_

write_checkpoint -force -noxdef fpmul_vio.dcp

catch { report_utilization -file fpmul_vio_utilization_synth.rpt -pb fpmul_vio_utilization_synth.pb }

if { [catch {
  file copy -force C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.runs/fpmul_vio_synth_1/fpmul_vio.dcp c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio.dcp
} _RESULT ] } { 
  send_msg_id runtcl-3 error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
  error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
}

if { [catch {
  write_verilog -force -mode synth_stub c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode synth_stub c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_verilog -force -mode funcsim c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode funcsim c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if {[file isdir C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.ip_user_files/ip/fpmul_vio]} {
  catch { 
    file copy -force c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_stub.v C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.ip_user_files/ip/fpmul_vio
  }
}

if {[file isdir C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.ip_user_files/ip/fpmul_vio]} {
  catch { 
    file copy -force c:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.srcs/sources_1/ip/fpmul_vio/fpmul_vio_stub.vhdl C:/Users/rohit/xilinx_project/fpmul_multicycle/fpmul/fpmul.ip_user_files/ip/fpmul_vio
  }
}

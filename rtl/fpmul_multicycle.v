// -------------------------------------------------------------------------------------------
// File Name     	: fpmul_multicycle.v
// Created By     	: Gaurav
// Date          	: 11/07/16
// Description   	: (a) This is IEEE single precision 32-bit floating point
//					  (b) This module improves the performance of the single cycle version in fpmul_algorithm.v
//						  by implementing multi-cycling method.
// -------------------------------------------------------------------------------------------

`timescale 1ns/1ps

module fpmul_multicycle (
  input clk,rst,start,
  input [31:0] a,b,
  output [31:0] p,
  output p_uf,p_of,p_nanf,p_inff,p_dnf,p_zf,done
  );

control_unit cu (clk,rst,start, normal,round,carry,uo_flow,map_bit23,nan,inf,zero,uflow,oflow,clr_flags,ld_inputs,ld_inflags1,ld_inflags2, ld_prod,ld_res_nan,ld_res_inf,ld_res_zero, norm_eap,incr_eap,calc_round,calc_carry,norm_map, ld_prod_flags,ld_prod_uflow,ld_prod_oflow,ld_res_reg,done);
data_path dp (clk,a,b, clr_flags,ld_inputs,ld_inflags1,ld_inflags2, ld_prod,ld_res_nan,ld_res_inf,ld_res_zero, norm_eap,incr_eap,calc_round,calc_carry,norm_map, ld_prod_flags,ld_prod_uflow,ld_prod_oflow,ld_res_reg, p, round,uflow,oflow,map_bit23, carry, normal,uo_flow, nan,inf,zero, p_uf,p_of,p_nanf,p_inff,p_dnf,p_zf);

endmodule

module control_unit (
  input clk,rst,start,
  input normal,round,carry,uo_flow,map_bit23,nan,inf,zero,uflow,oflow,
  output reg clr_flags,ld_inputs,ld_inflags1,ld_inflags2,
             ld_prod,ld_res_nan,ld_res_inf,ld_res_zero,
             norm_eap,incr_eap,calc_round,calc_carry,norm_map,
             ld_prod_flags,ld_prod_uflow,ld_prod_oflow,ld_res_reg,done
  );

localparam s0 = 4'h0,
           s1 = 4'h1,
           s2 = 4'h2,
           s3 = 4'h3,
           s4 = 4'h4,
           s5 = 4'h5,
           s6 = 4'h6,
           s7 = 4'h7,
           s8 = 4'h8,
           s9 = 4'h9,
          s10 = 4'ha,
          s11 = 4'hb,
          s12 = 4'hc,
          s13 = 4'hd;

reg [3:0] cs,ns;

//state register
always@(posedge clk or posedge rst)
begin
if (rst)
  cs <= s0;
else
  cs <= ns;
end

//next state combinational logic
always@(cs or start or normal or round or carry or uo_flow)
begin
  ns <= s0;
  case(cs)
  s0: ns <= s1;
  s1: begin
      if (start)
        ns <= s2;
      else
        ns <= s1;
      end
  s2: ns <= s3;
  s3: ns <= s4;
  s4: begin
      if (normal)
        ns <= s5;
      else
        ns <= s11;
      end
  s5: ns <= s6;
  s6: ns <= s7;
  s7: begin
      if (round)
        ns <= s8;
      else
        ns <= s9;
      end
  s8: begin
      if (carry)
        ns <= s9;
      else
        ns <= s10;
      end
  s9: ns <= s10;
  s10: begin
      if (uo_flow)
        ns <= s11;
      else
        ns <= s13;
      end
  s11: ns <= s12;
  s12: ns <= s13;
  s13: ns <= s1;
  default: ns <= s0;
  endcase
end

//current state combinational
always@(*)
begin
  clr_flags = 1'b0;
  ld_inputs = 1'b0;
  ld_inflags1 = 1'b0;
  ld_inflags2 = 1'b0;
  ld_prod = 1'b0;
  ld_res_nan = 1'b0;
  ld_res_inf = 1'b0;
  ld_res_zero = 1'b0;
  norm_eap = 1'b0;
  incr_eap = 1'b0;
  calc_round = 1'b0;
  calc_carry = 1'b0;
  norm_map = 1'b0;
  ld_prod_flags = 1'b0;
  ld_res_reg = 1'b0;
  ld_prod_uflow = 1'b0;
  ld_prod_oflow = 1'b0;
  done = 1'b0;
  case(cs)
  s0: clr_flags = 1'b1;
  s1: ld_inputs = 1'b1;
  s2: ld_inflags1 = 1'b1;
  s3: ld_inflags2 = 1'b1;
  s4: begin
      ld_prod = 1'b1;
      ld_res_nan = nan ? 1'b1 : 1'b0;
      ld_res_inf = inf ? 1'b1 : 1'b0;
      ld_res_zero = zero ? 1'b1 : 1'b0;
      end
  s5: begin
      norm_eap = 1'b1;
      incr_eap = map_bit23 ? 1'b1 : 1'b0;
      end
  s6: calc_round = 1'b1;
  s7: calc_carry = round ? 1'b1 : 1'b0;
  s8: norm_map = carry ? 1'b1 : 1'b0;
  s9: ld_prod_flags = 1'b1;
  s10: begin
       ld_res_reg = 1'b1;
       ld_prod_uflow = uflow ? 1'b1 : 1'b0;
       ld_prod_oflow = oflow ? 1'b1 : 1'b0;
       end
  s11: ld_prod_flags = 1'b1;
  s12: ld_res_reg = 1'b1;
  s13: done = 1'b1;
  default: begin
           clr_flags = 1'b0;
           ld_inputs = 1'b0;
           ld_inflags1 = 1'b0;
           ld_inflags2 = 1'b0;
           ld_prod = 1'b0;
           ld_res_nan = 1'b0;
           ld_res_inf = 1'b0;
           ld_res_zero = 1'b0;
           norm_eap = 1'b0;
           calc_round = 1'b0;
           calc_carry = 1'b0;
           norm_map = 1'b0;
           ld_prod_flags = 1'b0;
           ld_res_reg = 1'b0;
           ld_prod_uflow = 1'b0;
           ld_prod_oflow = 1'b0;
           done = 1'b0;
           end
  endcase
end
endmodule

module data_path (
  input clk,
  input [31:0] a,b,
  input clr_flags,ld_inputs,ld_inflags1,ld_inflags2,
        ld_prod,ld_res_nan,ld_res_inf,ld_res_zero,
        norm_eap,incr_eap,calc_round,calc_carry,norm_map,
        ld_prod_flags,ld_prod_uflow,ld_prod_oflow,ld_res_reg,
  output reg [31:0] p,
  output round,uflow,oflow,
  output reg carry,map_bit23,
  output normal,uo_flow,
  output reg nan,inf,zero,
  output reg p_uf,p_of,p_nanf,p_inff,p_dnf,p_zf
  );

reg sap,sb;
reg [9:0] eap;
reg [7:0] eb;
reg [23:0] map,mbl;
reg eap_zf,eap_hf,map_zf;
reg eb_zf,eb_hf,mbl_zf;
reg r,s;
wire ap_nanf,ap_inff,ap_zf;
wire b_nanf,b_inff,b_zf;

always@(posedge clk)
begin
if (clr_flags) 
  begin
  p <= 32'd0;
  p_uf <= 1'b0;
  p_of <= 1'b0;
  p_nanf <= 1'b0;
  p_inff <= 1'b0;
  p_dnf <= 1'b0;
  p_zf <= 1'b0;
  sap <= 1'b0;
  sb <= 1'b0;
  eap <= 10'b0;
  eb <= 8'b0;
  map <= 24'b0;
  mbl <= 24'b0;
  eap_zf <= 1'b0;
  eap_hf <= 1'b0;
  map_zf <= 1'b0;
  eb_zf <= 1'b0;
  eb_hf <= 1'b0;
  mbl_zf <= 1'b0;
  nan <= 1'b0;
  inf <= 1'b0;
  zero <= 1'b0;
  r <= 1'b0;
  s <= 1'b0;
  carry <= 1'b0;
  nan <= 1'b0;
  inf <= 1'b0;
  zero <= 1'b0;
  map_bit23 <= 1'b0;
  end
else if (ld_inputs)
  begin
  sap <= a[31];
  eap <= {2'b00,a[30:23]};
  map <= {1'b1,a[22:0]};
  sb <= b[31];
  eb <= b[30:23];
  mbl <= {1'b1,b[22:0]};
  end
else if (ld_inflags1)
  begin
  p_uf <= 1'b0;
  p_of <= 1'b0;
  eap_zf <= ~|(eap[7:0]);
  eap_hf <= &(eap[7:0]);
  map_zf <= ~|(map[22:0]);
  eb_zf <= ~|(eb[7:0]);
  eb_hf <= &(eb[7:0]);
  mbl_zf <= ~|(mbl[22:0]);
  sap <= sap ^ sb;
  eap <= eap + eb;
  end
else if (ld_inflags2)
  begin
  nan <= ap_nanf | b_nanf | (ap_inff & b_zf) | (ap_zf & b_inff);
  inf <= (ap_inff & ~(b_nanf | b_zf)) | (b_inff & ~(ap_nanf|ap_zf));
  zero <= (ap_zf & ~(b_nanf | b_inff)) | (b_zf & ~(ap_nanf | ap_inff));
  eap <= eap - 10'd127;
  end
else if (ld_prod)
  begin
  {map,mbl} <= map * mbl;
  map_bit23 <= map[23];
  if (ld_res_nan)
    begin
    eap[7:0] <= 8'hff;
    map <= 24'hffffff;
    end
  else if (ld_res_inf)
    begin
    eap[7:0] <= 8'hff;
    map <= 24'h000000;
    end
  else if (ld_res_zero)
    begin
    eap[7:0] <= 8'h00;
    map <= 24'h000000;
    end
  end
else if (norm_eap)
  begin
  if (incr_eap)
    begin
    eap <= eap + 1'b1;
    end
  else
    begin
    map <= {map[22:0],mbl[23]};
    mbl <= {mbl[22:0],1'b0};
    end
  end
else if (calc_round)
  begin
    r <= mbl[23];
    s <= |(mbl[22:0]);
  end
else if (calc_carry)
  begin
    {carry,map} <= map + 1'b1;
  end
else if (norm_map)
  begin
  eap <= eap + 1'b1;
  map <= {1'b1,map[23:1]};
  end
else if (ld_prod_flags)
  begin
  eap_zf <= ~|(eap[7:0]);
  eap_hf <= &(eap[7:0]);
  map_zf <= ~|(map[22:0]);
  end
else if (ld_res_reg) 
  begin
  if (ld_prod_uflow)
    begin
    eap[7:0] <= 8'h00;
    map <= 24'h000000;
    p_uf <= 1'b1;
    end
  else if (ld_prod_oflow)
    begin
    eap[7:0] <= 8'hff;
    map <= 24'h000000;
    p_of <= 1'b1;
    end
  p <= {sap,eap[7:0],map[22:0]};
  p_nanf <= eap_hf & ~map_zf;
  p_inff <= eap_hf & map_zf;
  p_dnf <= eap_zf & ~map_zf;
  p_zf <= eap_zf & map_zf;
  end
end

assign ap_nanf = eap_hf & ~map_zf;
assign ap_inff = eap_hf & map_zf;
assign ap_zf = eap_zf & map_zf;
assign b_nanf = eb_hf & ~mbl_zf;
assign b_inff = eb_hf & mbl_zf;
assign b_zf = eb_zf & mbl_zf;
assign round = r & (map[0] | s);
assign uflow = eap[9];
assign oflow = (~eap[9] & eap[8]) | eap_hf;
assign normal = ~(nan|inf|zero);
assign uo_flow = (uflow|oflow);

endmodule

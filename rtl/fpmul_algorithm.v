// -------------------------------------------------------------------------------------------
// File Name     	: fpmul_algorithm.v
// Created By     	: Gaurav
// Date          	: 11/07/16
// Description   	: (a) This is IEEE single precision 32-bit floating point
//					  (b) The current module is only for functional verification of algorithm
// -------------------------------------------------------------------------------------------


`timescale 1ns/1ps
module fpmul_algorithm (
  input clk,rst,start,
  input [31:0] din1,din2,
  output reg [31:0] dout,
  output reg done,uf,of,nanf,inff,dnf,zf
  );

reg [31:0] opa,opb,op_res;
wire f1,f2,f3,f4,f5,f6,finish;
reg valid;
wire [31:0] result;

always@(posedge clk or posedge rst)
begin
if (rst) begin
  opa <= 32'b0;
  opb <= 32'b0;
  valid <= 1'b0;
  end
else begin
  opa <= din1;
  opb <= din2;
  valid <= start;
  end
end

always@(posedge clk or posedge rst)
begin
if (rst) begin
  dout <= 32'b0;
  done <= 1'b0;
  uf <= 1'b0;
  of <= 1'b0;
  nanf <= 1'b0;
  inff <= 1'b0;
  dnf <= 1'b0;
  zf <= 1'b0;
  end
else begin
  dout <= result;
  done <= finish;
  uf <= f1;
  of <= f2;
  nanf <= f3;
  inff <= f4;
  dnf <= f5;
  zf <= f6;
  end
end

fpmul_comb inst_fpmul (opa,opb,valid,result,finish,f1,f2,f3,f4,f5,f6);

endmodule


module fpmul_comb (
  input [31:0] din1,
  input [31:0] din2,
  input start,
  output reg [31:0] dout,
  output reg done,
  output reg uf,of,nanf,inff,dnf,zf
  );

reg sA,sB,sC;
reg [7:0] expA,expB;
reg [22:0] fractA,fractB;
reg [9:0] expC;
reg [48:0] fractC;
reg lsb,guard,sticky,rnd;

always@(*)
begin
done = 0;
dout = 0;
inff = 0;
zf = 0;
uf = 0;
of = 0;
nanf = 0;
dnf = 0;
zf = 0;
sA = din1[31];
sB = din2[31];
expA = din1[30:23];
expB = din2[30:23];
fractA = din1[22:0];
fractB = din2[22:0];
if(start) begin
  inff = ((expA==8'hFF) && !fractA) || ((expB==8'hFF) && !fractB);
  zf = ((!expA && !fractA)||(!expB && !fractB));
  nanf = (((expA==8'hFF) && |fractA) || ((expB==8'hFF) && |fractB));
  dnf = ((!expA && |fractA)||(!expB && |fractB));
  if (inff)
    dout = {1'b0,8'b1,23'b0};
  else if (nanf)
    dout = {1'b0,8'b1,23'b1};
  else if (zf)
    dout = {1'b0,8'b0,23'b0};
  else if (dnf)
    dout = {1'b0,8'b0,23'b1};
  else
    begin
    sC = sA^sB;
    expC = {2'b00,expA} + {2'b00,expB} - 127;
    fractC = {1'b1,fractA}*{1'b1,fractB};
    if (fractC[47])
      expC = expC + 1;
    else
      fractC = fractC<<1;
    lsb = fractC[24];
    guard = fractC[23];
    sticky = |fractC[22:0];
    rnd = guard & (sticky|lsb);
    fractC[47:24] = fractC[47:24] + rnd;
    if (fractC[48])
      begin
      fractC[47:24] = fractC[48:25];
      expC = expC + 1;
      end
    uf = expC[9];
    of = !expC[9] && expC[8] || (expC[7:0]==8'hFF);
    inff = ((expC[7:0]==8'hFF) && !fractC[46:24]);
    zf = ((!expC[7:0] && !fractC[46:24]));
    nanf = ((expC[7:0]==8'hFF) && |fractC[46:24]);
    dnf = (!expC[7:0] && |fractC[46:24]);
    if (uf||zf)
      dout = 32'b0;
    else if (of||inff)
      dout = {1'b0,8'b1,23'b0};
    else if (nanf)
      dout = {1'b0,8'b1,23'b1};
    else if (dnf)
      dout = {1'b0,8'b0,23'b1};
    else
      dout = {sC,expC[7:0],fractC[46:24]};
    end
    done = 1;
  end
end
endmodule
